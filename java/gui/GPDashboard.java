package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Panel;
import java.awt.BorderLayout;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JButton;

public class GPDashboard {

	private JFrame frmGpdashboard;

	/**
	 * Launch the application.
	 */
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GPDashboard window = new GPDashboard();
					window.frmGpdashboard.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the application.
	 */
	public GPDashboard() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGpdashboard = new JFrame();
		frmGpdashboard.setTitle("GPDashboard");
		frmGpdashboard.setBounds(100, 100, 800, 600);
		frmGpdashboard.setVisible(true);
		frmGpdashboard.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Panel panel = new Panel();
		frmGpdashboard.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new MigLayout("", "[][][][][][][][][][][][][][][][][][][][][][][][][]", "[]"));
		
		JLabel lblWelcome = new JLabel("Welcome:");
		panel.add(lblWelcome, "cell 0 0");
		
		JLabel lblDes = new JLabel("Des");
		panel.add(lblDes, "cell 1 0");
		
		JButton btnLogOut = new JButton("Log out");
		panel.add(btnLogOut, "cell 24 0");
	}

}
