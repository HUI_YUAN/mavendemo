package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.JLayeredPane;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JSplitPane;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login {

	private JFrame frame;
	private JTextField UserName;
	private JPasswordField psd;
	private JRadioButton Admin;
	private JRadioButton Gp;
	private JRadioButton Phar;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("Calibri", Font.PLAIN, 15));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JLayeredPane layeredPane = new JLayeredPane();
		frame.getContentPane().add(layeredPane);
		
		JLayeredPane layeredPane_1 = new JLayeredPane();
		frame.getContentPane().add(layeredPane_1);
		layeredPane_1.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		layeredPane_1.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblAccountName = new JLabel("Account Name:");
		lblAccountName.setBounds(71, 40, 111, 24);
		panel.add(lblAccountName);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(71, 77, 111, 18);
		panel.add(lblPassword);
		
		JLabel lblUserType = new JLabel("User Type:");
		lblUserType.setBounds(71, 108, 111, 18);
		panel.add(lblUserType);
		
		UserName = new JTextField();
		UserName.setBounds(196, 40, 86, 24);
		panel.add(UserName);
		UserName.setColumns(10);
		
		psd = new JPasswordField();
		psd.setBounds(196, 74, 86, 24);
		panel.add(psd);
		
		ButtonGroup bg = new ButtonGroup();
		
		Admin = new JRadioButton("Admin");
		Admin.setBounds(159, 104, 69, 27);
		panel.add(Admin);
		bg.add(Admin);
		
		Gp = new JRadioButton("GP");
		Gp.setBounds(230, 104, 45, 27);
		panel.add(Gp);
		bg.add(Gp);
		
		Phar = new JRadioButton("Phar");
		Phar.setBounds(281, 104, 61, 27);
		panel.add(Phar);
		bg.add(Phar);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (Admin.isSelected()) {
					System.out.println(UserName.getText()+"\n"+psd.getText());
					//dao.login(UserName.getText(),psd.toString())
					//if (dao.login == true){
						AdminDashboard dash = new AdminDashboard();
						//System.out.println("work finish");
					//	}
				}else if (Gp.isSelected()) {
					GPDashboard dash = new GPDashboard(); 
				}else if (Phar.isSelected()) {
					PharDashboard dash = new PharDashboard();
				}
				frame.dispose();
			}
		});
		btnLogin.setBounds(196, 175, 86, 27);
		panel.add(btnLogin);
		
		JRadioButton rdbtnOtherStuff = new JRadioButton("Other Stuff");
		rdbtnOtherStuff.setBounds(159, 139, 117, 27);
		panel.add(rdbtnOtherStuff);
		bg.add(rdbtnOtherStuff);
		
		JRadioButton rdbtnPatient = new JRadioButton("Patient");
		rdbtnPatient.setBounds(281, 139, 157, 27);
		panel.add(rdbtnPatient);
		bg.add(rdbtnPatient);
	}
}
