package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Panel;
import java.awt.BorderLayout;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.BoxLayout;

public class PharDashboard {

	private JFrame frmPhardashboard;

	/**
	 * Launch the application.
	 */
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PharDashboard window = new PharDashboard();
					window.frmPhardashboard.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the application.
	 */
	public PharDashboard() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPhardashboard = new JFrame();
		frmPhardashboard.setTitle("PharDashboard");
		frmPhardashboard.setBounds(100, 100, 800, 600);
		frmPhardashboard.setVisible(true);
		frmPhardashboard.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Panel panel = new Panel();
		frmPhardashboard.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new MigLayout("", "[][][][][][][][][][][][][][][][][][][][][][][][]", "[]"));
		
		JLabel lblWelcome = new JLabel("Welcome:");
		panel.add(lblWelcome, "cell 0 0");
		
		JLabel lblNewLabel = new JLabel("Mr Des");
		panel.add(lblNewLabel, "cell 1 0");
		
		JButton btnLogOut = new JButton("Log out");
		panel.add(btnLogOut, "cell 23 0");
		
		Panel panel_1 = new Panel();
		frmPhardashboard.getContentPane().add(panel_1, BorderLayout.WEST);
		panel_1.setLayout(new MigLayout("", "[]", "[][][][][][]"));
		
		JButton btnCreatePrescription = new JButton("Create Prescription");
		panel_1.add(btnCreatePrescription, "cell 0 1");
		
		JButton btnViewPrescription = new JButton("View Prescription");
		panel_1.add(btnViewPrescription, "cell 0 3");
		
		JButton btnEditPrescription = new JButton("Edit Prescription");
		panel_1.add(btnEditPrescription, "cell 0 5");
	}

}
