package demo.MavenDemo;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class ApiTest {
	Api TestofApi;  
	@Test
	public void testAdd() {
		TestofApi = new Api();  
        int result = TestofApi.add(2, 3);  
        Assert.assertEquals("add is failed", 5, result);
		
	}

	@Test
	public void testSubtract() {
		TestofApi = new Api();  
        int result = TestofApi.subtract(12, 2); 
        Assert.assertEquals("substract is failed", 10, result);//set a wrong except value to test it
		
	}

	@Test
	public void testMultiply() {
		TestofApi = new Api();  
        int result = TestofApi.multiply(2, 3);  
        Assert.assertEquals("multiply is failed", 6, result);
	}

	@Test
	public void testDivide() {
		TestofApi = new Api();  
        int result = TestofApi.divide(3, 3);  
        Assert.assertEquals("divide is failed", 1, result);
	}

}
